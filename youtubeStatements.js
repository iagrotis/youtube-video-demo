/**
 * Actor and Context for statements that are not using Launch Server (LS)
 * */
var actor = {
    "mbox": "mailto:studen@uni.com",
    "name": "Test student",
    "objectType": "Agent"
};

var context = {
    "instructor": {
        "mbox": "mailto:example@test.com",
        "name": "test user"
    }
};

/**
 * Object for the statements
 */

var object =  {
    "id": "http://adlnet.gov/expapi/activities/example",
    "definition": {
        "name": {
            "en-US": "Example Youtube Video"
        },
        "description": {
            "en-US": "Example Youtube Video"
        }
    },
    "objectType": "Activity"
};

var started = false;
var seeking = false;
var prevTime = 0.0;
var completed = false;

function sendStatement(stmt_LRS) {
    const req = new XMLHttpRequest();
    req.open("POST",config.LRS_endpoint,async= false)
    req.setRequestHeader("Content-Type", "application/json;charset=utf-8");
    req.setRequestHeader("X-Experience-API-Version", "1.0.3");
    req.setRequestHeader("Access-Control-Allow-Origin", "*");
    req.setRequestHeader("Access-Control-Allow-Methods", "*");
    req.setRequestHeader("Authorization", config.Authorization);
    console.log("1: ",req);
    req.send(JSON.stringify(stmt_LRS));
    console.log("2: ",JSON.parse(req.responseText));
    console.log("3: ",stmt_LRS);
    return;
}

function sendStatementLS(LS_stmt) {
    console.log("Sending using LS");
    LS_stmt.context= {
        "instructor": {
            "mbox": "mailto:example@test.com",
            "name": "test user"
        },
        "contextActivities": {
            "grouping": [
                {
                    "definition": {
                        "name": {
                            "en-US": "Send from Launch Server"
                        }
                    },
                    "id": "http://adlnet.github.io/xapi-lab/index.html#context",
                    "objectType": "Activity"
                }
            ]
        }
    };
    const req = new XMLHttpRequest();
    console.log(`${LS_endpoint}/${token}`);
    req.open("POST",`${LS_endpoint}/${token}`,async= false);
    req.onreadystatechange = function(){
        console.log("State change: ",this.readyState);
        if(this.readyState== 5){
            console.log(req.responseText);
            console.log(req.status);
            if (req.status == "200"){
                console.log("Response: ",req.responseText);
            }else{
                console.log("Status: ",req.status);
                console.log("Response: ",req.responseText);
            }
        }
    };
   req.setRequestHeader("Content-Type", "application/json");
   req.setRequestHeader("Access-Control-Allow-Origin", "*");
   req.setRequestHeader("Access-Control-Allow-Methods", "*");
    console.log("4: ",JSON.stringify(LS_stmt));
    console.log("State : ",req.readyState);
    req.send(JSON.stringify(LS_stmt));
    console.log("5: ",LS_stmt);
    return;
}

function buildStatementLS(stmt) {
    if (stmt){
        var stmt = stmt;
        var actor = {
            "mbox": "mailto:studen@uni.com",
            "name": "Test student",
            "objectType": "Agent"
        };

        stmt.actor.mbox = `mailto:${actorLS.email}`;
        stmt.actor.name = actorLS.name;
        stmt.actor.objectType = "Agent";
        stmt.object = object;
        stmt.context = context;
    }
    return stmt;
}

function buildStatement(stmt) {
    if (stmt){
        var stmt = stmt;
        stmt.actor = actor;
        stmt.object = object;
        stmt.context = context;
    }
    return stmt;
}

function initializeVideo(ISOTime) {
    var stmt = {};
    stmt.verb = {
        id: videoprofile.references.initialized['@id'],
        display: {"en-US": "initialized"}
    };
    return buildStatement(stmt);
}

function playVideo(ISOTime) {
    var stmt = {};
    // calculate time from paused state
    var elapTime = (Date.now() - prevTime) / 1000.0;
    if (!started || elapTime > 0.2) {
        console.log("yt: playing");
        stmt.verb = {
        id: videoprofile.verbs.played['@id'],
        display: videoprofile.verbs.played.prefLabel
        };
        stmt.result = {"extensions":{"resultExt:resumed":ISOTime}};
        started = true;
        sendStatement(buildStatement(stmt));
        sendStatementLS(buildStatementLS(stmt));
    }
    else {
        log("yt: seeking");
        seeking = true;
        return seekVideo(ISOTime);
    }
    return buildStatement(stmt);
}

function pauseVideo(ISOTime) {
    var stmt = {};
    seeking = false;
    // check for seeking
    if (!seeking) {
        stmt.verb = {
        id: videoprofile.verbs.paused['@id'],
        display: videoprofile.verbs.paused.prefLabel
        };
        stmt.result = {"extensions":{"resultExt:paused":ISOTime}};
        // manually send 'paused' statement because of interval delay
        sendStatement(buildStatement(stmt));
        sendStatementLS(buildStatementLS(stmt));
    }
    else {
        seeking = false;
    }
}

function seekVideo(ISOTime) {
    var stmt = {};
    stmt.verb = {
        id: videoprofile.verbs.seeked['@id'],
        display: videoprofile.verbs.seeked.prefLabel
    }
    stmt.result = {"extensions":{"resultExt:seeked":ISOTime}};
    sendStatement(buildStatement(stmt));
    sendStatementLS(buildStatementLS(stmt));
}

function completeVideo(ISOTime) {
    if (completed) {
        return null;
    }
    var stmt = {};
    stmt.verb = {
        id: videoprofile.references.completed['@id'],
        display: {"en-US": "completed"}
    }
    stmt.result = {"duration":ISOTime, "completion": true};
    completed = true;
    sendStatement(buildStatement(stmt));
    sendStatementLS(buildStatementLS(stmt));
}

function exitVideo() {
    if (!started) {
        return;
    }
    var stmt = {};
    var e = "";
    // 'terminated' statement for completed video
    if (completed) {
        e = "terminated";
        stmt.verb = {
        id: videoprofile.references.terminated['@id'],
        display: { "en-US": "terminated" }
        };
        // 'abandoned' statement for incomplete video
    } else {    e = "abandoned";
        stmt.verb = {
        id: videoprofile.references.abandoned['@id'],
        display: { "en-US": "abandoned" }
        };
    }
    // send statement immediately to avoid event delay
    sendStatement(buildStatement(stmt));
    sendStatementLS(buildStatementLS(stmt));
}
